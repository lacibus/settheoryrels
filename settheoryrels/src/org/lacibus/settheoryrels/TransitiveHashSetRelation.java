  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 
 * A transitive relation implemented using a hash set of ordered
 * pairs of related elements.
 * 
 * Note that, for this class to work, equal members of the
 * base class must have the same hash code. This will generally
 * mean that, if the equals() function is over-ridden for class T
 * then the default implementation of hashCode() must be over-
 * ridden also. 
 *
 * The OrderedPair class over-rides hashCode() and combines
 * the hash codes of the elements of the pair to create the
 * pair hash codes. If equal elements have the same hash codes
 * then so will equal pairs.
 * 
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 *
 */

public class TransitiveHashSetRelation<T> 
					extends HashSetRelation<T>
					implements Relation<T>, Serializable {

	private static final long serialVersionUID = -4194178202667652047L;
	
	/**
	 * The set of all ordered pairs of elements for which the
	 * second element follows the first element.
	 * 
	 * This set includes all the related pairs that are
	 * implied by the stated relationships, as well as
	 * those that have been explicitly stated.
	 */
	
	protected Set<OrderedPair<T>> impliedRelationships =
					new HashSet<OrderedPair<T>>();

	public synchronized boolean removeElement(T e) {
		
		if (removeElementStatements(e)) {		
			regenerateImpliedRelationships();		
			return true;
		}
		else return false;
	}

	public synchronized boolean isFollowedBy(T e1, T e2) {
		return impliedRelationships.contains(
						new OrderedPair<T>(e1, e2));
	}
	
	@Override
	public synchronized Set<T> followersOf(T e) {
		Set<T> result = new HashSet<T>();
		for (OrderedPair<T> p : impliedRelationships) {
			if (p.firstElement.equals(e))
				result.add(p.secondElement);
		}
		return result;
	}

	@Override
	public synchronized Set<T> followedBy(T e) {
		Set<T> result = new HashSet<T>();
		for (OrderedPair<T> p : impliedRelationships) {
			if (p.secondElement.equals(e))
				result.add(p.firstElement);
		}
		return result;
	}

	public synchronized boolean stateRelationship(T e1, T e2) {
		
		if (statedRelationships.add(new OrderedPair<T>(e1, e2))) {
			baseSet.add(e1);
			baseSet.add(e2);
			addWithImplications(e1, e2);
			return true;
		}
		else return false;
	}
	
	public synchronized boolean removeStatedRelationship(T e1, T e2)  {
		
		if (statedRelationships.remove(new OrderedPair<T>(e1, e2))) {
			regenerateImpliedRelationships();
			return true;
		}
		else return false;
	}
	
	public synchronized List<OrderedPair<T>> relatedPairs() {
		List<OrderedPair<T>> l = new ArrayList<OrderedPair<T>>();
		for (OrderedPair<T> p : impliedRelationships)  l.add(p);
		return l;
	}
		
	public synchronized Set<OrderedPair<T>> relatedPairsSet() {
		return new HashSet<OrderedPair<T>>(impliedRelationships);
	}
		
	/**
	 * Regenerate the implied relationships from the base set
	 * and the stated relationships
	 */
	
	protected void regenerateImpliedRelationships() {
		
		impliedRelationships = new HashSet<OrderedPair<T>>();
		for (OrderedPair<T> p : statedRelationships) 
					addWithImplications(p.firstElement, p.secondElement);
	}

	protected void addWithImplications(T e1, T e2) {
		addWithTransitiveImplications(e1, e2);
	}

	protected void addWithTransitiveImplications(T e1, T e2) {

		// Find the elements that are or are followed by the first element
		ArrayList<T> e1Preceders = new ArrayList<T>();
		e1Preceders.add(e1);
		for (OrderedPair<T> p : impliedRelationships) {
			if (p.secondElement.equals(e1))
				e1Preceders.add(p.firstElement);
		}
		
		// Find the elements that are or that follow the second element
		ArrayList<T> e2Followers = new ArrayList<T>();
		e2Followers.add(e2);
		for (OrderedPair<T> p : impliedRelationships) {
			if (p.firstElement.equals(e2))
				e2Followers.add(p.secondElement);
		}
		
		// By implication, each of the elements that is or is followed 
		// by the first element is followed by each of the elements 
		// that is or that follows the second element 
		for (T e1p : e1Preceders) {
			for (T e2f : e2Followers) {
				impliedRelationships.add(new OrderedPair<T>(e1p, e2f));
			}
		}
	}

}
