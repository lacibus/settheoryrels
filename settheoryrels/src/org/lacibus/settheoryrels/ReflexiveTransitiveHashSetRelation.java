  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.io.Serializable;
import java.util.HashSet;

/**
 * 
 * A reflexive and transitive relation implemented using
 * a HashSet of the related ordered pairs of elements.
 * 
 * Note that, for this class to work, equal members of the
 * base class must have the same hash code. This will generally
 * mean that, if the equals() function is over-ridden for class T
 * then the default implementation of hashCode() must be over-
 * ridden also. 
 *
 * The OrderedPair class over-rides hashCode() and combines
 * the hash codes of the elements of the pair to create the
 * pair hash codes. If equal elements have the same hash codes
 * then so will equal pairs.
 * 
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 */

public class ReflexiveTransitiveHashSetRelation<T> 
				extends TransitiveHashSetRelation<T>
				implements Relation<T>, Serializable {
	
	private static final long serialVersionUID = -3122940960363929371L;

	public synchronized boolean addElement(T e) {
		if (baseSet.add(e)) {
			addWithImplications(e, e);
			return true;
		}
		else return false;
	}

	public synchronized boolean stateRelationship(T e1, T e2) {
		
		if (statedRelationships.add(new OrderedPair<T>(e1, e2))) {
			baseSet.add(e1);
			baseSet.add(e2);
			addWithImplications(e1, e1);
			addWithImplications(e2, e2);
			addWithImplications(e1, e2);
			return true;
		}
		else return false;
	}
	
	protected void regenerateImpliedRelationships() {
		
		impliedRelationships = new HashSet<OrderedPair<T>>();
		for (T e: baseSet) 
			impliedRelationships.add(new OrderedPair<T>(e, e));
		for (OrderedPair<T> p : statedRelationships) 
					addWithImplications(p.firstElement, p.secondElement);
	}

}
