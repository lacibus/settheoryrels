  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.util.List;
import java.util.Set;

/**
 * 
 * A relation on a set of elements that are instances of a class.
 * 
 * A relation is defined in set theory to be a set of ordered
 * pairs of elements. 
 * 
 * In a pair of related elements, the second element is said for
 * the purpose of this java package to "follow" the first element.
 * 
 * The relation may be a "deducing relation" - one in which
 * relationships between elements are deduced from a set of stated
 * relationships because of some inherent property of the relation
 * (for example, that it is transitive).
 *
 * When an element is added to or removed from the set on
 * which a deducing relation is defined, the inherent property of
 * the relation may lead to the deduction of some relationships.
 * For example, if the relation is symmetric then it can be
 * deduced that the element follows itself.
 * 
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 */

public interface Relation<T> {

	/**
	 * 
	 * Add an element to the set on which the relation is defined.
	 * 
	 * @param element the element to be added.
	 * 
	 * @return true if the element was added, false if it was not
	 * added because it was already in the set. The set will have
	 * changed when this method returns if, and only if, it returns
	 * true.
	 * 
	 */
	
	public abstract boolean addElement(T element);
	
	/**
	 * 
	 * Remove an element from the set on which the relation is defined.
	 * 
	 * All relationships that the element has are removed.
	 *  
	 * @param element the element to be removed
	 * 
	 * @return true if the element was in the set  on which the
	 * relation is defined, false otherwise. In either case, the
	 * set will not contain the element when this method returns.
	 * 
	 */
	
	public abstract boolean removeElement(T element);
	
	/**
	 * 
	 * Determine whether an instance of the base class is a
	 * member of the set on which the relation is defined.
	 * 
	 * @param instance the instance to be checked
	 * 
	 * @return true if and only if the instance is a
	 * member of the set on which the relation is defined.
	 * 
	 */
	
	public abstract boolean isElement(T instance);
	
	/**
	 * 
	 * Get the set on which the relation is defined.
	 * 
	 * @return a set that contains exactly those elements that are
	 * members of the set on which the relation is defined. Changes
	 * to the returned set will not alter the base set itself.
	 * 
	 */
	
	public abstract Set<T> baseSet();
		
	/**
	 * Determine whether one element is stated to be followed
	 * by another
	 * 
	 * @param e1 an element
	 * 
	 * @param e2 an element
	 * 
	 * @return true if e1 is stated to be followed by e2,
	 * false otherwise
	 * 
	 */
	
	public boolean isStatedToBeFollowedBy(T e1, T e2);
		
	/**
	 * Determine whether one element is followed by another
	 * 
	 * @param e1 an element
	 * 
	 * @param e2 an element
	 * 
	 * @return true if e1 is followed by e2, false otherwise
	 * 
	 */
	
	public abstract boolean isFollowedBy(T e1, T e2);
	
	/**
	 * Get the set of elements that follow a given element
	 * 
	 * @param e an element
	 * 
	 * @return a set containing just those elements that
	 * follow the given element.
	 * 
	 */
	
	public abstract Set<T> followersOf(T e);
	
	/**
	 * Get the set of elements that are followed by
	 * a given element
	 * 
	 * @param e an element
	 * 
	 * @return a set containing just those elements that
	 * are followed by the given element.
	 * 
	 */
	
	public abstract Set<T> followedBy(T e);

	/**
	 * State a relationship between two elements.
	 * 
	 * The ordered pair of the first and second elements is
	 * added to the set of stated relationships and the
	 * elements are added to the set on which the relation
	 * is defined if they are not in it already.
	 * 
	 * @param e1 the first element
	 * 
	 * @param e2 the second element
	 * 
	 * @return true if the ordered pair of the first and second
	 * elements was added to the set of stated relationships,
	 * false if it was already there.
	 * 
	 */
	
	public abstract boolean stateRelationship(T e1, T e2);
	
	/**
	 * Remove a stated relationship between two elements.
	 * 
	 * The ordered pair of the first and second elements is
	 * removed from the set of stated relationships. Any
	 * deduced relationships whose deduction depends on the
	 * removed stated relationship are removed also.
	 * 
	 * The elements in the removed stated relationship are
	 * not removed from the set on which the relation is
	 * defined.
	 * 
	 * @param e1 the first element
	 * 
	 * @param e2 the second element
	 * 
	 * @return true if the pair of elements was a stated
	 * relationship, false otherwise. In either case, the
	 * set of stated relationships will not contain the pair
	 * of elements when this method returns.
	 */
	
	public abstract boolean removeStatedRelationship(T e1, T e2);
	
	/**
	 * List of the ordered pairs of elements that have been stated
	 * to be related
	 * 
	 * @return a list of the ordered pairs of elements that have
	 * been stated to be related
	 */
	
	public List<OrderedPair<T>> statedRelatedPairs();
	
	/**
	 * Set of the ordered pairs of elements that have been stated
	 * to be related
	 * 
	 * @return a set of the ordered pairs of elements that have
	 * been stated to be related
	 */
	
	public Set<OrderedPair<T>> statedRelatedPairsSet();
	
	/**
	 * List of the ordered pairs of elements that are related
	 * 
	 * @return a list of the ordered pairs of elements that are related.
	 * 
	 */
	
	public abstract List<OrderedPair<T>> relatedPairs();
		
	/**
	 * Set of the ordered pairs of elements that are related
	 * 
	 * @return a set of the ordered pairs of elements that are related.
	 * 
	 */
	
	public abstract Set<OrderedPair<T>> relatedPairsSet();
		
}
