  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 
 * A relation implemented using a HashSet of the related
 * ordered pairs of elements.
 * 
 * Note that, for this class to work, equal members of the
 * base class must have the same hash code. This will generally
 * mean that, if the equals() function is over-ridden for class T
 * then the default implementation of hashCode() must be over-
 * ridden also. 
 *
 * The OrderedPair class over-rides hashCode() and combines
 * the hash codes of the elements of the pair to create the
 * pair hash codes. If equal elements have the same hash codes
 * then so will equal pairs.
 * 
 * This is not a deducing relationship. There are no implied
 * relationships other than those that have been stated.
 *
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 */

public class HashSetRelation<T> implements Relation<T>, Serializable {

	private static final long serialVersionUID = -9001415732119066633L;

	/**
	 * The set of elements on which the relation is defined.
	 */
	
	Set<T> baseSet = new HashSet<T>();

	/**
	 * The set of ordered pairs of elements for which the
	 * second element has been stated to follow the first
	 * element.
	 */
	
	Set<OrderedPair<T>> statedRelationships =
			new HashSet<OrderedPair<T>>();
	
	/**
	 * The set of all ordered pairs of elements for which the
	 * second element follows the first element.
	 * 
	 * This set includes all the related pairs that are
	 * implied by the stated relationships, as well as
	 * those that have been explicitly stated.
	 */
	
	Set<OrderedPair<T>> impliedRelationships =
			new HashSet<OrderedPair<T>>();

	@Override
	public synchronized boolean addElement(T element) {
		return baseSet.add(element);
	}

	@Override
	public synchronized boolean removeElement(T e) {
		return removeElementStatements(e);
	}
		
	@Override
	public synchronized boolean isElement(T instance) {
		return baseSet.contains(instance);
	}

	@Override
	public synchronized Set<T> baseSet() {
		return new HashSet<T>(baseSet);
	}

	protected boolean removeElementStatements(T e) {
		if (baseSet.remove(e)) {
			Iterator<OrderedPair<T>> it = statedRelationships.iterator();
			while (it.hasNext()) {
				OrderedPair<T> p = it.next();
				if (p.firstElement.equals(e) ||
					p.secondElement.equals(e))
				it.remove();
			}
			return true;
		}
		else return false;
	}
	
	public synchronized boolean isStatedToBeFollowedBy(T e1, T e2) {
		
		return statedRelationships.contains(new OrderedPair<T>(e1, e2));
	}
	
	public synchronized boolean isFollowedBy(T e1, T e2) {
		
		return statedRelationships.contains(new OrderedPair<T>(e1, e2));
	}
	
	@Override
	public synchronized Set<T> followersOf(T e) {
		Set<T> result = new HashSet<T>();
		for (OrderedPair<T> p : statedRelationships) {
			if (p.firstElement.equals(e))
				result.add(p.secondElement);
		}
		return result;
	}

	@Override
	public synchronized Set<T> followedBy(T e) {
		Set<T> result = new HashSet<T>();
		for (OrderedPair<T> p : statedRelationships) {
			if (p.secondElement.equals(e))
				result.add(p.firstElement);
		}
		return result;
	}

	public synchronized boolean stateRelationship(T e1, T e2) {
		
		if (statedRelationships.add(new OrderedPair<T>(e1, e2))) {
			baseSet.add(e1);
			baseSet.add(e2);
			return true;
		}
		else return false;
	}
	
	public synchronized boolean removeStatedRelationship(T e1, T e2)  {
		
		return statedRelationships.remove(new OrderedPair<T>(e1, e2));
	}

	@Override
	public List<OrderedPair<T>> statedRelatedPairs() {
		List<OrderedPair<T>> l = new ArrayList<OrderedPair<T>>();
		for (OrderedPair<T> p : statedRelationships)  l.add(p);
		return l;
	}

	@Override
	public Set<OrderedPair<T>> statedRelatedPairsSet() {
		return new HashSet<OrderedPair<T>>(statedRelationships);
	}

	@Override
	public synchronized List<OrderedPair<T>> relatedPairs() {
		return statedRelatedPairs();
	}

	@Override
	public Set<OrderedPair<T>> relatedPairsSet() {
		return new HashSet<OrderedPair<T>>(statedRelationships);
	}

}
