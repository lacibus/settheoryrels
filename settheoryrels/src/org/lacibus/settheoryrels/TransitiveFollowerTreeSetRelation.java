  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.util.Map.Entry;
import java.io.Serializable;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * 
 * A transitive relation implemented using, for each element,
 * a TreeSet of the elements that have been deduced to follow it.
 * 
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 */

public class TransitiveFollowerTreeSetRelation<T> 
						extends FollowerTreeSetRelation<T>
						implements Relation<T>, Serializable {

	private static final long serialVersionUID = 367222442296667098L;

	protected void regenerateRelationships() {
		renewFollowersMap();
		addAllTransitiveImplications();
	}
	
	protected void renewFollowersMap() {
		Set<T> baseSet = new TreeSet<T>(followersMap.keySet());
		followersMap = new TreeMap<T, Set<T>>();
		for (T e : baseSet) followersMap.put(e, new TreeSet<T>());		
	}
	
	protected void addImplicationsOfRelation(T e1, T e2) {
		addTransitiveImplications(e1, e2);
	}
		
	protected void addAllTransitiveImplications() {
		for (OrderedPair<T> p: statedRelationships) 
			addTransitiveImplications(
					p.firstElement, p.secondElement);
	}

	protected void addTransitiveImplications(T e1, T e2) {

		// Create a set containing e1 and the entries that are
		// followed by e1, and a set containing e2 and
		// the entries that follow e2
		Set<T> e1AndFollowedByE1 =new TreeSet<T>();
		e1AndFollowedByE1.add(e1);
		Set<T> e2AndFollowersOfE2 =new TreeSet<T>();
		e2AndFollowersOfE2.add(e2);
		for (Entry<T, Set<T>> mapping : followersMap.entrySet()) {
			if (mapping.getValue().contains(e1))
				e1AndFollowedByE1.add(mapping.getKey());
			if (mapping.getKey().equals(e2)) {
				Set<T> s = mapping.getValue();
				for (T e : s) e2AndFollowersOfE2.add(e);
			}
		}
		
		// e2 and every element that follows e2 must follow
		// e1 and every element that follows e1
		for (T ea : e1AndFollowedByE1) {
			Set<T> eaFollowers = followersMap.get(ea);
			for (T ez : e2AndFollowersOfE2) {
				eaFollowers.add(ez);
			}
		}
	}
	
}
