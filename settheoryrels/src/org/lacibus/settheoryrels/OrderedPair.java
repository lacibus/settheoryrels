  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.io.Serializable;

public class OrderedPair<T> 
				implements Comparable<OrderedPair<T>>, Serializable {
	
	private static final long serialVersionUID = -2112587414358242631L;
	
	public T firstElement;
	public T secondElement;
	
	public OrderedPair(T first, 
			T second) { 
		firstElement = first;
		secondElement = second;
	}
	
	public boolean equals(Object o) {
		try {
			@SuppressWarnings("unchecked")
			OrderedPair<T> other =
					(OrderedPair<T>)o;
			return ((secondElement.equals(other.secondElement)) &&
					(firstElement.equals(other.firstElement)));
		}
		catch (Exception e) {
			return false;
		}
	}
	
	public int hashCode() {
		
		int h1 = firstElement.hashCode();
		int h2 = secondElement.hashCode();
		return ((h1 << 16) ^ (h1 & 0Xffff0000)) | 
			   (((h1 & 0Xffff0000) >>> 16) ^ (h2 & 0Xffff));
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public int compareTo(OrderedPair<T> p) {
		int c = ((Comparable<T>)firstElement).compareTo(p.firstElement);
		if (c != 0) return c;
		return ((Comparable<T>)secondElement).compareTo(p.secondElement);
	}
	
}

