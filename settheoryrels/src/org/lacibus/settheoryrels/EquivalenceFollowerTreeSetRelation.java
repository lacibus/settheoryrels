  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.io.Serializable;

/**
 * 
 * A symmetric, reflexive and transitive relation implemented using,
 * for each element, a TreeSet of the elements that have been deduced
 * to follow it.
 * 
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 */

public class EquivalenceFollowerTreeSetRelation<T> 
						extends ReflexiveTransitiveFollowerTreeSetRelation<T>
						implements Relation<T>, Serializable {

	private static final long serialVersionUID = 5192924489961708680L;

	protected void regenerateRelationships() {
		renewFollowersMap();
		addAllReflexiveImplications();
		addAllSymmetricImplications();
		addAllTransitiveImplications();		
	}
	
	protected void addImplicationsOfRelation(T e1, T e2) {
		followersMap.get(e2).add(e1);
		addTransitiveImplications(e1, e2);
		addTransitiveImplications(e2, e1);
	}
	
	protected void addAllSymmetricImplications() {
		for (OrderedPair<T> p: statedRelationships) {
			followersMap.get(p.secondElement).add(p.firstElement);
			addTransitiveImplications(
					p.secondElement, p.firstElement);
		}
	}
	
}
