  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * 
 * A relation implemented using, for each element, a TreeSet of
 * the elements that have been deduced to follow it.
 * 
 * This is not a deducing relationship. There are no implied
 * relationships other than those that have been stated.
 *
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 */

public class FollowerTreeSetRelation<T> implements Relation<T>, Serializable {

	private static final long serialVersionUID = 7511036766504226602L;

	/**
	 * The set of ordered pairs of stated relationships.
	 */
	
	Set<OrderedPair<T>> statedRelationships =
			new TreeSet<OrderedPair<T>>();

	/**
	 * The map between the elements on which the relation is defined
	 * and the sets of elements that have been deduced to follow them.
	 */
	
	Map<T, Set<T>> followersMap = new TreeMap<T, Set<T>>();

	@Override
	public synchronized boolean addElement(T e) {
		if (followersMap.containsKey(e)) return false;	
			// Element already in base set
		followersMap.put(e, new TreeSet<T>());
		addImplicationsOfElement(e);
		return true;
	}

	@Override
	public synchronized boolean removeElement(T e) {
		if (followersMap.remove(e) == null) return false;
		Iterator<OrderedPair<T>> it = statedRelationships.iterator();
		while (it.hasNext()) {
			OrderedPair<T> p = it.next();
			if (p.firstElement.equals(e) ||
				p.secondElement.equals(e)) it.remove();
		}
		Collection<Set<T>> followerSets = followersMap.values();
		for (Set<T> followerSet : followerSets) 
			followerSet.remove(e);
		regenerateRelationships();
		return true;
	}
	
	@Override
	public synchronized boolean isElement(T instance) {
		return followersMap.containsKey(instance);
	}

	@Override
	public synchronized Set<T> baseSet() {
		return new TreeSet<T>(followersMap.keySet());
	}
	
	public synchronized boolean isStatedToBeFollowedBy(T e1, T e2) {
		
		return statedRelationships.contains(new OrderedPair<T>(e1, e2));
	}
	
	public synchronized boolean isFollowedBy(T e1, T e2) {
		
		Set<T> followers = followersMap.get(e1);
		if (followers == null) return false;
		return followers.contains(e2);
	}
	
	@Override
	public synchronized Set<T> followersOf(T e) {
		Set<T> followers = followersMap.get(e);
		if (followers == null) return new TreeSet<T>();
		else return new TreeSet<T>(followers);
	}

	@Override
	public synchronized Set<T> followedBy(T e) {
		Set<T> result = new TreeSet<T>();
		for (Entry<T, Set<T>> entry : followersMap.entrySet()) {
			if (entry.getValue().contains(e))
				result.add(entry.getKey());
		}
		return result;
	}

	public synchronized boolean stateRelationship(T e1, T e2) {
		addElement(e1);
		addElement(e2);
		if (statedRelationships.add(new OrderedPair<T>(e1, e2))) {
			followersMap.get(e1).add(e2);
			addImplicationsOfRelation(e1, e2);
			return true;
		}
		else return false;
	}
	
	public synchronized boolean removeStatedRelationship(T e1, T e2)  {
		
		if (statedRelationships.remove(new OrderedPair<T>(e1, e2))) {
			followersMap.get(e1).remove(e2);
			regenerateRelationships();
			return true;
		}
		return false;
	}
		
	@Override
	public synchronized List<OrderedPair<T>> statedRelatedPairs() {
		List<OrderedPair<T>> l = new ArrayList<OrderedPair<T>>();
		for (OrderedPair<T> p : statedRelationships) {
			l.add(new OrderedPair<T>(p.firstElement, p.secondElement));
		}
		return l;
	}
	
	@Override
	public Set<OrderedPair<T>> statedRelatedPairsSet() {
		return new TreeSet<OrderedPair<T>>(statedRelationships);
	}
	
	@Override
	public synchronized List<OrderedPair<T>> relatedPairs() {
		List<OrderedPair<T>> l = new ArrayList<OrderedPair<T>>();
		Set<Entry<T, Set<T>>> followerMappings = followersMap.entrySet();
		for (Entry<T, Set<T>> m : followerMappings) {
			T e1 = m.getKey();
			Set<T> followers = m.getValue();
			for (T e2 : followers) l.add(new OrderedPair<T>(e1, e2));
		}
		return l;
	}
	
	@Override
	public Set<OrderedPair<T>> relatedPairsSet() {
		Set<OrderedPair<T>> s = new TreeSet<OrderedPair<T>>();
		Set<Entry<T, Set<T>>> followerMappings = followersMap.entrySet();
		for (Entry<T, Set<T>> m : followerMappings) {
			T e1 = m.getKey();
			Set<T> followers = m.getValue();
			for (T e2 : followers) s.add(new OrderedPair<T>(e1, e2));
		}
		return s;
	}

	protected void addImplicationsOfElement(T e) {}

	protected void regenerateRelationships() {}
	
	protected void addImplicationsOfRelation(T e1, T e2) {}

}
