/**
 * This package contains simple java classes implementing basic concepts of relations as
 * defined in set theory.
 * 
 * A relation is a set of ordered pairs. A relation can have properties, including that
 * it is reflexive, symmetric, or transitive. The classes enable some ordered pairs to be
 * stated, and will deduce all the other related ordered pairs implied by the relation's
 * properties. There are implementations of transitive, reflexive-and-transitive, and
 * symmetric-reflexive-and-transitive (equivalence) relations using hash sets and tree
 * sets.
 **/

package org.lacibus.settheoryrels;
