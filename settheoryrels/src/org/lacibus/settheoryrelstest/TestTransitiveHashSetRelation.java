  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrelstest;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.lacibus.settheoryrels.OrderedPair;
import org.lacibus.settheoryrels.TransitiveHashSetRelation;

/**
 * 
 * Implementation of transitive HashSet relation that is intended
 * to be used in testing. It extends the TransitiveHashSetRelation
 * class and overrides its addWithTransitiveImplications method
 * with a method that uses a different algorithm, less effective,
 * but more obviously correct.
 *
 * @param <T> the class of which the elements of the set on which
 * the relation is defined are instances.
 * 
 */

public class TestTransitiveHashSetRelation<T>
				extends TransitiveHashSetRelation<T>
				implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2447691811585045889L;

	protected void addWithTransitiveImplications(T e1, T e2) {
		Set<OrderedPair<T>> newRelationships = 
				new HashSet<OrderedPair<T>>();
		newRelationships.add(new OrderedPair<T>(e1, e2));
		addTransitiveImplications(newRelationships);
	}

	protected void addTransitiveImplications(
			Set<OrderedPair<T>> newRelationships) {

		Set<OrderedPair<T>> additions = newRelationships;
		do {
			boolean done = true;
			for (OrderedPair<T> p : additions) {
				if (impliedRelationships.add(p)) done = false;
			}
			if (done) break;
			additions = new HashSet<OrderedPair<T>>();
			for (OrderedPair<T> p1 : impliedRelationships) {
				for (OrderedPair<T> p2 : impliedRelationships) {
					if (p2.firstElement.equals(p1.secondElement)) 
						additions.add(new OrderedPair<T>(
								p1.firstElement, p2.secondElement));
				}
			}			
		} while (true);
	}

}
