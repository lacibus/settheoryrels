  /*
   * Copyright (c) 2018, Lacibus Ltd
   * 
   * All rights reserved.
   *
   * Redistribution and use in source and binary forms, with or without modification, are permitted provided that
   * the following conditions are met:
   *
   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer
   * in the documentation and/or other materials provided with the distribution.
   *
   * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
   * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
   * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
   * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
   * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
   * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
   *
   */

package org.lacibus.settheoryrelstest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.lacibus.settheoryrels.EquivalenceHashSetRelation;
import org.lacibus.settheoryrels.OrderedPair;
import org.lacibus.settheoryrels.Relation;

public class Tester implements Serializable { 
	
	private static final long serialVersionUID = -2953858740622449636L;
	
	static Relation<TestElement> relation1 =
			new TestEquivalenceHashSetRelation<TestElement>();
	static Relation<TestElement> relation2 =
			new EquivalenceHashSetRelation<TestElement>();
	
	static TestElement[] testElements;
	
	static int nrOfTests;

	static Random random = new Random();
	
	static int errorCount = 0;
	static PrintWriter log;
	static long startMs = 0;
	
	/**
	 * @param args the arguments passed when the program
	 * is invoked.
	 * 
	 * 1. The number of test elements
	 * 2. The number of tests
	 */
	
	public static void main(String[] args) {
		
		generateTestElements(args[0]);
		nrOfTests = getNrOfTests(args[1]);
		System.out.println("Starting " + nrOfTests + " tests " + 
						   " with " + args[0] + " elements.");
		startLog();
		for (int i = 0; i < nrOfTests; i++) performTest();
		endLog();
	}

	static void generateTestElements(String s) {
		int n = 0;
		try {
			n = Integer.parseInt(s);
		}
		catch (Exception e) {
			n = 0;
		}
		if (n <= 0) {
			System.err.println("Invalid number of test elements");
			System.exit(-1);
		}
		testElements = new TestElement[n];
		for (int i = 0; i < n; i++) {
			boolean ok;
			int nr;
			do {
				ok = true;
				nr = random.nextInt();
				for (int j = 0; j < i; j++) {
					if (testElements[j].value == nr) {
						ok = false;
						break;
					}
				}
			} while (!ok);
			testElements[i] = new TestElement(nr);
		}
	}
	
	static int getNrOfTests(String s) {
		int n = 0;
		try {
			n = Integer.parseInt(s);
		}
		catch (Exception e) {
			n = 0;
		}
		if (n <= 0) {
			System.err.println("Invalid number of tests");
			System.exit(-1);
		}
		return n;
	}
	
	static void startLog() {
		File f = new File("/Users/chrisharding/tmp/testlog.txt");
		try {
			BufferedWriter w = new BufferedWriter(
					new FileWriter(f)) ;
			log = new PrintWriter(w);
			startMs = System.currentTimeMillis();
			log.println("Starting tests at " + new Date(startMs));
		} catch (IOException e) {
			System.err.println("Failed to create logfile");
			System.exit(-1);
		}
	}
	
	static void endLog() {
		log.println("Tests complete with " + errorCount + " errors after " + 
						(System.currentTimeMillis() - startMs) + "ms.");
		log.close();
		System.err.println("Tests complete with " + errorCount + " errors after " + 
				(System.currentTimeMillis() - startMs) + "ms.");
	}
	
	static void performTest() {
		int t = random.nextInt(11);
		switch(t) {
		case 0 : testAddElement(); break;
		case 1 : testRemoveElement(); break;
		case 2 : testCheckElement(); break;
		case 3 : testBaseSet(); break;
		case 4 : testStateRelationship(); break;
		case 5 : testIsStatedToBeFollowedBy(); break;
		case 6 : testFollowersOf(); break;
		case 7 : testFollowedBy(); break;
		case 8 : testStatedRelatedPairs(); break;
		case 9 : testIsFollowedBy(); break;
		case 10 : testRelatedPairs(); break;
		}
	}
	
	static void error(String message) {
		errorCount++;
		log.println("ERROR: " + message);
	}
	
	static void testAddElement() {
		TestElement e = testElements[random.nextInt(testElements.length)];
		boolean result1 = relation1.addElement(e);
		boolean result2 = relation2.addElement(e);
		log.println("Added element " + e.value);
		if (result1 != result2) error("Add element mismatch: " + result1 + 
				", " + result2 + " for element " + e.value);		
	}

	static void testRemoveElement() {
		TestElement e = testElements[random.nextInt(testElements.length)];
		boolean result1 = relation1.removeElement(e);
		boolean result2 = relation2.removeElement(e);
		log.println("Removed element " + e.value);
		if (result1 != result2) error("Remove element mismatch: " + result1 + 
				", " + result2 + " for element " + e.value);
	}

	static void testCheckElement() {
		TestElement e = testElements[random.nextInt(testElements.length)];
		boolean result1 = relation1.isElement(e);
		boolean result2 = relation2.isElement(e);
		log.println("Checked element " + e.value);
		if (result1 != result2) error("Check element mismatch: " + result1 + 
				", " + result2 + " for element " + e.value);
	}

	static void testBaseSet() {
		if (random.nextInt(10) != 0) return;
		Set<TestElement> result1 = relation1.baseSet();
		Set<TestElement> result2 = relation2.baseSet();
		log.println("Checked base sets.");
		if (!result1.equals(result2)) error("Base set mismatch");
	}

	static void testStateRelationship() {
		TestElement e1 = testElements[random.nextInt(testElements.length)];
		TestElement e2 = testElements[random.nextInt(testElements.length)];
		boolean result1 = relation1.stateRelationship(e1, e2);
		boolean result2 = relation2.stateRelationship(e1, e2);
		log.println("Checked state relationship " +
						e1.value + ", " + e2.value);
		if (result1 != result2) error("State relationship mismatch: " + result1 + 
				", " + result2 + " for elements " +
				e1.value + ", " + e2.value);
	}

	static void testIsStatedToBeFollowedBy() {
		TestElement e1 = testElements[random.nextInt(testElements.length)];
		TestElement e2 = testElements[random.nextInt(testElements.length)];
		boolean result1 = relation1.isStatedToBeFollowedBy(e1, e2);
		boolean result2 = relation2.isStatedToBeFollowedBy(e1, e2);
		log.println("Checked stated to be followed by " +
						e1.value + ", " + e2.value);
		if (result1 != result2) error("Stated to be followed by mismatch: " + result1 + 
				", " + result2 + " for elements " +
				e1.value + ", " + e2.value);
	}

	static void testStatedRelatedPairs() {
		if (random.nextInt(10) != 0) return;
		List<OrderedPair<TestElement>> result1 = relation1.statedRelatedPairs();
		Collections.sort(result1);
		List<OrderedPair<TestElement>> result2 = relation2.statedRelatedPairs();
		Collections.sort(result2);
		log.println("Checked stated related pairs.");
		compareLists(result1, result2, "Stated related pairs mismatch");
	}

	static void testIsFollowedBy() {
		TestElement e1 = testElements[random.nextInt(testElements.length)];
		TestElement e2 = testElements[random.nextInt(testElements.length)];
		boolean result1 = relation1.isFollowedBy(e1, e2);
		boolean result2 = relation2.isFollowedBy(e1, e2);
		log.println("Checked followed by " +
						e1.value + ", " + e2.value);
		if (result1 != result2) error("Followed by mismatch: " + result1 + 
				", " + result2 + " for elements " +
				e1.value + ", " + e2.value);
	}
	
	static void testFollowersOf() {
		TestElement e = testElements[random.nextInt(testElements.length)];
		Set<TestElement> result1 = relation1.followersOf(e);
		Set<TestElement> result2 = relation2.followersOf(e);
		log.println("Checked followers of " + e.value);
		if (!result1.equals(result2)) error("FollowersOf  mismatch for " +
												e.value);
	}

	static void testFollowedBy() {
		TestElement e = testElements[random.nextInt(testElements.length)];
		Set<TestElement> result1 = relation1.followedBy(e);
		Set<TestElement> result2 = relation2.followedBy(e);
		log.println("Checked followed by " + e.value);
		if (!result1.equals(result2)) error("FollowedBy  mismatch for " +
												e.value);
	}

	static void testRelatedPairs() {
		if (random.nextInt(10) != 0) return;
		List<OrderedPair<TestElement>> result1 = relation1.relatedPairs();
		Collections.sort(result1);
		List<OrderedPair<TestElement>> result2 = relation2.relatedPairs();
		Collections.sort(result2);
		log.println("Checked related pairs.");
		compareLists(result1, result2, "Related pairs mismatch");
	}

	static void compareLists(List<OrderedPair<TestElement>> list1,
			List<OrderedPair<TestElement>> list2, String message) {
		Iterator<OrderedPair<TestElement>> it1 = list1.iterator();
		Iterator<OrderedPair<TestElement>> it2 = list2.iterator();
		boolean mismatch = false;
		while (it1.hasNext() && it2.hasNext()) {
			OrderedPair<TestElement> p1 = it1.next();
			OrderedPair<TestElement> p2 = it2.next();
			if (!p1.equals(p2)) {
				error(message + ": starting with " +
						p1.firstElement.value + "," + p1.secondElement.value + ";" +
						p2.firstElement.value + "," + p2.secondElement.value);
				mismatch = true;
				break;
			}
		}
		if (!mismatch) {
			if (it1.hasNext()) {
				OrderedPair<TestElement> p1 = it1.next();
				error(message + ": starting with " +
						p1.firstElement.value + "," + p1.secondElement.value + "; null");

			}
			else if (it2.hasNext()) {
				OrderedPair<TestElement> p2 = it2.next();
				error(message + ": starting with null;" +
						p2.firstElement.value + "," + p2.secondElement.value);				
			}
		}
	}
}
